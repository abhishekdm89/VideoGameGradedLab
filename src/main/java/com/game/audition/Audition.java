package com.game.audition;

import java.util.ArrayList;
import java.util.List;

public class Audition {

	private Performer performer;
	private List<Performer> performerList;
	
	public Audition() {
		this.performerList = new ArrayList<Performer>();
	}
	
	public void addPerformer(String performerType, String id) {
		performer = new Performers();
		performer.setUnionId(id);
		performerList.add(performer);
	}

	public void addPerformer(String performerType, String id, String style) {
		
		if(performerType.equalsIgnoreCase("dancer"))	{
			performer = new Dancer();
			this.addDancer(id, style);
		}
		else if(performerType.equalsIgnoreCase("vocalist")) {
			performer = new Vocalist(id, style);
			
		}
		performerList.add(performer);
	
	}
	
	public void addPerformer(String performerType, String id, String style, int volume) {
		
			performer = new Vocalist();
			this.addVocalist(id, style, volume);
		
		performerList.add(performer);
	
	}
	
	private void addDancer(String id, String style) {
		// TODO Auto-generated method stub
		this.performer.setUnionId(id);
		this.performer.setStyle(style);
	}

	private void addVocalist(String id, String keyNote, int volume) {
		// TODO Auto-generated method stub
		performer.setUnionId(id);
		performer.setStyle(keyNote);
		Vocalist vocalist = (Vocalist) performer;
		vocalist.setVolume(volume);
		performer = vocalist;
	} 
	
	public void printAuditionList() {
		for(Performer performer : this.performerList) {
			if(performer instanceof Vocalist) {
				Vocalist vocalist = (Vocalist) performer;
				int singVolume = 0;
				if(vocalist.getVolume() != -1) {
					singVolume = vocalist.getVolume();
					System.out.println("I sing in the key of - "+vocalist.getStyle()+"- at volume "+singVolume+" -"+vocalist.getUnionId());
				}else {
					System.out.println("I sing in the key of - "+vocalist.getStyle()+"-"+vocalist.getUnionId());
				}
				
			}
			else if(performer instanceof Dancer) 
			{
				System.out.println(performer.getStyle()+"- "+performer.getUnionId()+" - dancer");
			}
			else {
				System.out.println(performer.getUnionId() + " - Performer");
			}
		}
	}

	public List<Performer> getPerformerList() {
		
		return this.performerList;
	}
	
	

}
