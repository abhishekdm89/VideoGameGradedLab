package com.game.audition;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

public class DancerTest {

	private Dancer dancer;
	private String expectedUnionId;
	private String actualUnionId;
	private String expectedDanceStyle;

	@Before
	public void setUp() {
		dancer = new Dancer("Salsa");
	}
	
	@Test
	public void testUnionId() {
		dancer.setUnionId("111");
		this.actualUnionId = dancer.getUnionId();
		this.expectedUnionId = dancer.getUnionId();
		assertEquals(this.expectedUnionId, this.actualUnionId);
	}
	
	@Test
	public void getDanceStyle() {
		this.expectedDanceStyle = "Salsa";
		dancer.setStyle("Salsa");
	}
	
}
