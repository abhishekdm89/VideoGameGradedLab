package com.game.audition;

import static org.junit.Assert.*;
import org.junit.Before;

import org.junit.Test;

public class PerformersTest {

	private Performer performers;
	private String expectedId;
	private String actualId;

	@Before
	public void setUp() {
		performers = new Performers("989");
		
	}
	
	@Test
	public void testUnionId() {
		this.expectedId = "989";
		this.actualId = performers.getUnionId();
		assertEquals(expectedId, actualId);
	}
}
