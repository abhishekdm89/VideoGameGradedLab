package com.game.audition;

import static org.junit.Assert.*;
import org.junit.Before;

import org.junit.Test;

public class VocalistTest {

	private Vocalist vocalist;
	private String expectedKey;
	private String actualKey;
	private String actualUnionId;
	private String expectedUnionId;
	private int expectedVolume;
	private int actualVolume;
	private Vocalist vocalist1;
	private String expectedKeyNote;
	private String actualKeyNote;

	@Before
	public void setUp() {
		vocalist = new Vocalist("123", "G", 4);
		vocalist1 = new Vocalist("456", "K");
	}
	
	@Test
	public void testKey() {
		this.expectedKey = "G";
		vocalist.setStyle("G");
		this.actualKey = vocalist.getStyle();
		assertEquals(this.expectedKey, this.actualKey);
	}
	
	@Test
	public void testUnionId() {
		this.actualUnionId = vocalist.getUnionId();
		this.expectedUnionId = vocalist.getUnionId();
		assertEquals(this.expectedUnionId, this.actualUnionId);
	}
	
	@Test
	public void testVolumeVocalist() {
		this.expectedVolume = 4;
		this.actualVolume = vocalist.getVolume();
		assertEquals(this.expectedVolume, this.actualVolume);
	}
	
	@Test
	public void testKeyNoteVocalist1() {
		this.expectedKeyNote = "K";
		this.actualKeyNote = vocalist1.getStyle();
		assertEquals(this.expectedKeyNote, this.actualKeyNote);
	}
}
