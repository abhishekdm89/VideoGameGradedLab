package com.game.audition;

import java.util.UUID;

public class Vocalist implements Performer {
	private String key;
	private String unionId;
	private int volume;
	
	public Vocalist() {
	}
	
	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public Vocalist(String unionId, String key, int volume) {
		this.unionId = unionId;
		this.key = key;
		this.volume = volume;
		if(key.length() > 1 || key == null) {
			throw new IllegalArgumentException("The key cannot be either null or cannot have multiple characters");
		}
		if(volume < 1 || volume > 10) {
			throw new IllegalArgumentException("The volume has to be between 1 & 10");
		}
	}
	
	public Vocalist(String unionId, String key) {
		this.unionId = unionId;
		this.key = key;
		this.volume = -1;
		if(key.length() > 1 || key == null) {
			throw new IllegalArgumentException("The key cannot be either null or cannot have multiple characters");
		}
	}

	public void setUnionId(String id) {
		// TODO Auto-generated method stub
		this.unionId = id;
	}

	public String getStyle() {
		
		// TODO Auto-generated method stub
		return this.key;
	}

	public void setStyle(String keyNote) {
		// TODO Auto-generated method stub
		this.key = keyNote;
		if(key.length() > 1 || key == null) {
			throw new IllegalArgumentException("The key cannot be either null or cannot have multiple characters");
		}
	}

	public String getUnionId() {
		// TODO Auto-generated method stub
		return this.unionId;
	}

	

}
